{
  imports = [ <nixpkgs/nixos/modules/virtualisation/google-compute-image.nix> ];
  users.users.root.openssh.authorizedKeys.keyFiles = [ ~/.ssh/id_rsa.pub ];
}
